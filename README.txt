
Information about the theme
===========================
# The Images folder houses custom graphics specific to this theme. You can put additional theme related images here.
# The ishalist CSS file contains the majority of the CSS that influences the layout and color scheme
# The screenshot.png thumbnail will appear on the /admin/build/theme page

Tips:
=====
# Make use of the body classes, you can get very granular with your CSS using it
# If you stack the boxes, it probably a good idea to make the content all the same height
# You should probably have content in all boxes or none.
# Read the directions about using the maintenance-page.tpl - See below
# You should overwrite the default favion
# Two ways to add a footer: You can add a block to the footer section on /admin/build/block or add one on /admin/settings/site-information
# To prevent the "river of news" effect do not promote more than one post at a time to front

# Using the maintenance-page.tpl
# 1. Go to sites/default/settings.php (Make writable if necessary)
# 2. Around line 170, add the line code - Read notes in the settings.php page
# 3. You are all set, make any customizations to the template

############### Copy Code Below ###########
                                           
$conf['maintenance_theme'] = 'ishalist';  //
                                         
############## End Code Copy ##############

# Tested with most browsers: Firefox 3, IE5.5-IE8, Safari, Chrome, Opera
