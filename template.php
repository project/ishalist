<?php

/** 
 * This file contains theme override functions and preprocess functions
 * If you prefer the Drupal default rendering simply comment out the functions.
 * Additionally you can add your own custom functions and variables here
 */

/**
 * Custom Breadcrumb
 */
function ishalist_breadcrumb($breadcrumb) {
  $title = check_plain(drupal_get_title());
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' / ', $breadcrumb). ' / ' . $title .'</div>';
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Get rid of the default "Submitted by.." text on nodes
 */ 
function ishalist_node_submitted($node) {
  return t('Posted by !username on @datetime',
    array(
      '!username' => theme('username', $node),
      '@datetime' => format_date($node->created, 'custom', 'M j, Y'),
    ));
}

/**
 * Get rid of the default "Submitted by.." text for comments
 */ 
function ishalist_comment_submitted($comment) {
  return t('comment posted on @datetime',
    array(
      '!username' => theme('username', $comment),
      '@datetime' => format_date($comment->timestamp, 'custom', 'M j, Y')
    ));
}

/**
 * Register comment form theme function
 */
function ishalist_theme() {
  return array(
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Customizations to the comment form.
 *
 */
function ishalist_comment_form($form) {
  // Replaces "Subject:" on comment form
  $form['subject']['#title'] = t('Headline');
  
  // Replaces "Comment:" on comment form
  $form['comment_filter']['comment']['#title'] = t('Message');
  
  // Removes preview button
  //$form['preview'] = NULL;
    
  // Comment submit button
  $form['submit']['#value'] = t('Submit comment');
    
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme comment_form header
 */
function ishalist_preprocess_box(&$vars, $hook) {
  switch($vars['title']) {
   case 'Post new comment':
    $vars['title'] = t('Leave a comment or suggestion...');
  }
}